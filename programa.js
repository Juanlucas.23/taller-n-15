function mapa(){

    let mapa = L.map('mapa',{center: [4.751152, -74.062952],zoom: 15});
    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    
    mosaico.addTo(mapa); 

    let barrio = L.polygon([
        [4.755510558967133,-74.0657714009285],
        [4.747892536397094,-74.06874060630798],
        [4.747443471386982,-74.06726002693176],
        [4.747507623549183,-74.06668066978453],
        [4.746823333511531,-74.06128406524658],
        [4.746620184775969,-74.05976593494415],
        [4.753885387884816,-74.05812442302704],
        [4.755510558967133,-74.0657714009285],
    ]).addTo(mapa);

    let marcador = L.marker([4.751152, -74.062952]);
    marcador.addTo(mapa);
}